package com.example.thelaststep.model;

import java.util.ArrayList;

public class FoodData {
    public static String [][] data = new String[][]{
            {"Rendang","Sumatera Barat", "Dilihat dari gambarnya saja rendang sudah lezat, apalagi saat kamu mencicipinya. Kelezatan rendang sudah dikenal dunia. Bahan utama rendang adalah daging. Dimasak dengan aneka bumbu khas Indonesia, rasa rendang menjadi tiada duanya. Banyak orang yang langsung ketagihan pada saat pertama kali mereka mencicipi rendang.", "https://uprint.id/blog/wp-content/uploads/2016/11/makanan-indonesia-04.jpg"},
            {"Sate", "Jawa", "Sate, siapa yang tidak suka makan sate? Makanan yang menggunakan tusuk bambu dan menghasilkan bau harum saat dibakar ini adalah makanan kesukaan banyak orang Indonesia. Jenis sate ada bermacam-macam, seperti sate ayam, babi dan kambing.", "https://uprint.id/blog/wp-content/uploads/2016/11/makanan-indonesia-05.jpg"},
            {"Gado - gado", "Jakarta","Gado-gado adalah makanan khas Indonesia yang sehat. Kamu bisa lihat dari gambar makanan di atas, isinya adalah sayur-sayuran. Dicampur dengan bumbu kacang super lezat membuat gado-gado menjadi salah satu makanan berisi sayuran yang paling banyak disukai.","https://uprint.id/blog/wp-content/uploads/2016/11/makanan-indonesia-01.jpg"},
            {"Bika Ambon", "Maluku","Ada kue khas Indonesia yang rasanya disukai banyak orang. Nama kue tersebut adalah bika ambon. Bika ambon terbuat dari bahan-bahan utama pembuat kue, seperti telur, gula dan santan.","https://uprint.id/blog/wp-content/uploads/2016/11/makanan-indonesia-06.jpg"},
            {"Pempek", "Sumatera Selatan", "Apa jadinya jika ikan dan tepung diadon menjadi satu lalu digoreng? Pempek adalah sebutan untuk makanan ini. Pempek biasanya dimakan menggunakan bumbunya yang terbuat dari cuka yang dicampur dengan gula.","https://uprint.id/blog/wp-content/uploads/2016/11/makanan-indonesia-03.jpg"},
            {"Nasi Goreng","Seluruh Indonesia","Nasi goreng juga ada di negara-negara lain, namun bagaimanapun juga nasi goreng Indonesia terasa lebih enak di lidah karena memang dimasak menggunakan bumbu-bumbu khas Indonesia yang sangat kaya.","https://uprint.id/blog/wp-content/uploads/2016/11/makanan-indonesia-07.jpg"}
    };

    public static ArrayList<Food> getListFoodData(){
        Food food = null;
        ArrayList<Food> list = new ArrayList<>();
        for (String[] foods : data){
            food = new Food();
            food.setName(foods[0]);
            food.setRemarks(foods[1]);
            food.setDetails(foods[2]);
            food.setImage(foods[3]);

            list.add(food);
        }
        return list;
    }
}
