package com.example.thelaststep.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.thelaststep.R;
import com.example.thelaststep.model.Food;

import java.util.ArrayList;

public class GridFoodAdapter extends RecyclerView.Adapter<GridFoodAdapter.GridViewHolder> {
    private Context context;
    private ArrayList<Food> list;

    public GridFoodAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Food> getList() {
        return list;
    }

    public void setList(ArrayList<Food> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public GridFoodAdapter.GridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_grid_food, viewGroup, false);
        return new GridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GridFoodAdapter.GridViewHolder gridViewHolder, int i) {
        Glide.with(context)
                .load(list.get(i).getImage())
                .apply(new RequestOptions().override(350,550))
                .into(gridViewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class GridViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public GridViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_grid_item_photo);
        }
    }
}
