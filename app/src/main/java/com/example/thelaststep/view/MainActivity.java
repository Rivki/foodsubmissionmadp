package com.example.thelaststep.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.thelaststep.R;
import com.example.thelaststep.adapter.CardFoodAdapter;
import com.example.thelaststep.adapter.GridFoodAdapter;
import com.example.thelaststep.adapter.ListFoodAdapter;
import com.example.thelaststep.model.Food;
import com.example.thelaststep.model.FoodData;
import com.example.thelaststep.utils.ItemClickSupport;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvFood;
    private ArrayList<Food> list = new ArrayList<>();
    private String title = "Mode List";
    final String STATE_TITLE = "state_string";
    final String STATE_LIST = "state_list";
    final String STATE_MODE = "state_mode";
    int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.init();

        if (savedInstanceState == null) {
            setTitleBar("Mode List");
            list.addAll(FoodData.getListFoodData());
            showRecycleList();
            mode = R.id.list_action;
        } else {
            String stateTitle = savedInstanceState.getString(STATE_TITLE);
            ArrayList<Food> stateList = savedInstanceState.getParcelableArrayList(STATE_LIST);
            int stateMode = savedInstanceState.getInt(STATE_MODE);
            setTitleBar(stateTitle);
            list.addAll(stateList);
            setMode(stateMode);
        }
    }

    private void init() {
        rvFood = findViewById(R.id.idRV);
        rvFood.setHasFixedSize(true);

        showRecycleList();


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TITLE, getSupportActionBar().getTitle().toString());
        outState.putParcelableArrayList(STATE_LIST, list);
        outState.putInt(STATE_MODE, mode);
    }

    private void showRecycleList() {
        rvFood.setLayoutManager(new LinearLayoutManager(this));
        final ListFoodAdapter listFoodAdapter = new ListFoodAdapter(this);
        listFoodAdapter.setList(list);
        rvFood.setAdapter(listFoodAdapter);

        ItemClickSupport.addTo(rvFood).setmOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                showSelectedFood(list.get(position));
                Food f = listFoodAdapter.getList().get(position);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Bundle b = new Bundle();
                b.putParcelable("food", f);
                intent.putExtra("food", b);
                startActivity(intent);
            }
        });
    }

    private void showGridFoodList() {
        rvFood.setLayoutManager(new LinearLayoutManager(this));
        final GridFoodAdapter gridFoodAdapter = new GridFoodAdapter(this);
        gridFoodAdapter.setList(list);
        rvFood.setAdapter(gridFoodAdapter);

        ItemClickSupport.addTo(rvFood).setmOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Food f = gridFoodAdapter.getList().get(position);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Bundle b = new Bundle();
                b.putParcelable("food", f);
                intent.putExtra("food", b);
                startActivity(intent);
            }
        });
    }

    private void showCardFoodList() {
        rvFood.setLayoutManager(new LinearLayoutManager(this));
        CardFoodAdapter cardFoodAdapter = new CardFoodAdapter(this, this);
        cardFoodAdapter.setList(list);
        rvFood.setAdapter(cardFoodAdapter);
    }

    private void setTitleBar(String title) {
        getSupportActionBar().setTitle(title);
    }

//    private void showSelectedFood(Food food){
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        this.setMode(item.getItemId());

        return super.onOptionsItemSelected(item);
    }

    public void setMode(int selectedMode) {
        String title = null;
        switch (selectedMode) {
            case R.id.list_action:
                title = "List Item Nusantara Foods";
                this.showRecycleList();
                break;
            case R.id.card_action:
                title = "Card Item Nusantara Foods";
                this.showCardFoodList();
                break;
            case R.id.grid_action:
                title = "Grid Item Nusantara Foods";
                this.showGridFoodList();
                break;
        }
        mode = selectedMode;
        setTitleBar(title);
    }
}
