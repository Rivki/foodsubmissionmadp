package com.example.thelaststep.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.thelaststep.R;
import com.example.thelaststep.model.Food;
import com.example.thelaststep.utils.CustomOnItemClickListener;
import com.example.thelaststep.view.DetailActivity;
import com.example.thelaststep.view.MainActivity;

import java.util.ArrayList;

public class CardFoodAdapter extends RecyclerView.Adapter<CardFoodAdapter.CardViewHolder> {
    private Context context;
    private ArrayList<Food> list;
    private MainActivity ma;

    public CardFoodAdapter(Context context, MainActivity ma) {
        this.context = context;
        this.ma = ma;
    }

    public ArrayList<Food> getList() {
        return list;
    }

    public void setList(ArrayList<Food> list) {
        this.list = list;
    }



    @NonNull
    @Override
    public CardFoodAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card_food, viewGroup, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardFoodAdapter.CardViewHolder cardViewHolder, int i) {
        Food f = getList().get(i);

        Glide.with(context)
                .load(f.getImage())
                .apply(new RequestOptions().override(350,550))
                .into(cardViewHolder.imgPhoto);

        cardViewHolder.tvName.setText(f.getName());
        cardViewHolder.tvRemarks.setText(f.getRemarks());

        cardViewHolder.btnDetail.setOnClickListener(new CustomOnItemClickListener(i, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                Food f = getList().get(position);
                Intent intent = new Intent(ma, DetailActivity.class);
                Bundle b = new Bundle();
                b.putParcelable("food", f);
                intent.putExtra("food", b);
                ma.startActivity(intent);
            }
        }));
    }

    @Override
    public int getItemCount() {
        return getList().size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvRemarks;
        ImageView imgPhoto;
        Button btnDetail;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_item_card_name);
            tvRemarks = itemView.findViewById(R.id.tv_item_card_remarks);
            imgPhoto = itemView.findViewById(R.id.img_item_card_photo);
            btnDetail = itemView.findViewById(R.id.btn_card_detail);
        }
    }
}
