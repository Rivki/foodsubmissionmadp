package com.example.thelaststep.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.thelaststep.R;
import com.example.thelaststep.model.Food;

import java.util.ArrayList;

public class ListFoodAdapter extends RecyclerView.Adapter<ListFoodAdapter.CategoryViewHolder> {
    private Context context;
    private ArrayList<Food> list;

    public ListFoodAdapter(Context context) {
        this.context = context;
    }


    public ArrayList<Food> getList() {
        return list;
    }

    public void setList(ArrayList<Food> list) {
        this.list = list;
    }


    @NonNull
    @Override
    public ListFoodAdapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_food, viewGroup, false);
        return new CategoryViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(@NonNull ListFoodAdapter.CategoryViewHolder categoryViewHolder, int i) {
        categoryViewHolder.tvName.setText(getList().get(i).getName());
        categoryViewHolder.tvRemarks.setText(getList().get(i).getRemarks());

        Glide.with(context)
                .load(getList().get(i).getImage())
                .apply(new RequestOptions().override(55,55))
                .into(categoryViewHolder.imgFood);
    }

    @Override
    public int getItemCount() {
        return getList().size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvRemarks;
        ImageView imgFood;

        CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvRemarks = itemView.findViewById(R.id.tv_item_remarks);
            imgFood = itemView.findViewById(R.id.img_item_photo);
        }
    }
}
