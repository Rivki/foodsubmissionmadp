package com.example.thelaststep.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.thelaststep.R;
import com.example.thelaststep.model.Food;

public class DetailActivity extends AppCompatActivity {

    private TextView tvDetailName, tvDetailRemarks, tvDetailText;
    private ImageView imgDetail;
    private Context context;
    private Food food;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle bundle = getIntent().getBundleExtra("food");
        food = bundle.getParcelable("food");

        try {
            tvDetailName = findViewById(R.id.tv_detail_name);
            tvDetailName.setText(food.getName());

            tvDetailRemarks = findViewById(R.id.tv_detail_remarks);
            tvDetailRemarks.setText(food.getRemarks());

            tvDetailText = findViewById(R.id.tv_detail_detail);
            tvDetailText.setText(food.getDetails());

            imgDetail = findViewById(R.id.img_detail);
            String receiveImage = food.getImage();
            Glide.with(this)
                    .load(receiveImage)
                    .apply(new RequestOptions().override(350, 550))
                    .into(imgDetail);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
